# README #

TedCrawler get all comments from all talks from TED.com

### How does it work ###

It gets talks data from TED API and for each talk it looks for threadId, which is hidden in recesses of html code (e.g. http://www.ted.com/talks/lesley_hazleton_the_doubt_essential_to_faith). 
Once the threadId is obtained it gets all comments using unofficial TED API and stores them up in database.

### Configuration ###
Create *connection_string.txt* and put a connection string there, e.g. 
*postgresql://user:password@localhost/test*

Create *api_key.txt* with key for TED API.

Then run **createDatabase.py**.

To get all comments run **talks.py**. 

### Issues ###
Once in a while a *processComment* function in **crawler.py** gets invalid comment. I don't know why.