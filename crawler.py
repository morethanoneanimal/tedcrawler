import requests
import time
import sys
import json
import sqlalchemy
import db
import datetime

class Crawler:
	def __init__(self):
		self.session = requests.Session()
		self.session.headers['Accept'] = 'application/json, text/javascript, */*; q=0.01'
		self.session.headers['Connection'] = 'keep-alive'
		self.session.headers['X-Requested-With'] = 'XMLHttpRequest'
		self.db = db.getDb()
		self.comments = sqlalchemy.Table('comments', sqlalchemy.MetaData(self.db), autoload=True)
		self.errors = 0

	def processComment(self, comment, conversationId, parentId = None):
		insert = self.comments.insert()
		if 'comment_id' not in comment:
			#print "wtf wtf wtf"
			#print comment
			self.errors += 1
			return

		idx = comment['comment_id']
		# uzupelnianie pol
		insert.execute(commentId = idx,
				name = comment['name'],
				content = comment['comment'],
				deleted = comment['deleted'],
                profileId = comment['profile_id'],
                talkId = conversationId,
				parentCommentId = parentId,
				date = datetime.datetime.fromtimestamp(int(comment['date'])),
				score = comment['score'],
			replies = comment['replies'])
		if len(comment['children']) > 0:
			for child in comment['children'].values():
				self.processComment(child, conversationId, idx)

	def getComments(self, talkId):
		page = 1
		while page < 150: # tak na wszelki wypadek
			response = self.session.get('http://www.ted.com/conversation_forums/%d?page=%d&per_page=100&sort=newest' % (talkId, page))
			if response.text == 'null': return False
			js = json.loads(response.text)
			if js['discussion_thread']['counts']['comments'] == 0:
				break
			conversationId = js['conversation_id']
			for comment in js['discussion_thread']['thread'][0]:
				record = js['discussion_thread']['thread'][0][comment]
				self.processComment(record, conversationId)
			page += 1
		return True

#crawler= Crawler()
#crawler.getComments(958)
