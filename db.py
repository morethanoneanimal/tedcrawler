import sqlalchemy

def getDb():
	with open('connection_string.txt', 'r') as cs:
		return sqlalchemy.create_engine(cs.read(), echo=False)
