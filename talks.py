import json
import requests
import re
from crawler import Crawler

session = requests.Session()

with open('api_key.txt', 'r') as f:
	apikey = f.read()[:-1]

def getUrl(offset):
	return 'https://api.ted.com/v1/talks.json?api-key=' + apikey +'&limit=100&offset=' + str(offset)
def getTalkUrl(talk):
	return 'http://ted.com/talks/' + talk['slug']

# ugly, I know
def getThreadId(talk, tries = 3):
	if tries == 0: return -1
	try:
		content = session.get(getTalkUrl(talk)).text
		match = re.search( r'"threadId":(\d+)', content)
		if match:
			return match.group(1)
		else: return -1
	except Exception as e:
		return getThreadId(talk, tries-1)

offset = 0
crawler = Crawler()
counter = 0

while True:
	content = session.get(getUrl(offset)).text
	offset += 100
	js = json.loads(content)
	print js['counts']	
	if js['counts']['this'] == 0:
		break

	for talk in js['talks']:
		try:
			talk = talk['talk']
			threadId = int(getThreadId(talk))
			print "getting comments for talk " + talk['name'] + " id: " + str(talk['id']) + ", threadId: " + str(threadId) + " (errors so far: " + str(crawler.errors) + ")"
			if threadId < 0:
				print "error, skipping"
				continue
			result = crawler.getComments(threadId)
			if not result:
				print "error, threadId invalid?"
			else: counter += 1
		except Exception as e:
			print e
			print talk


print "over, errors: " + str(crawler.errors)
print "talks covered: " + str(counter)
