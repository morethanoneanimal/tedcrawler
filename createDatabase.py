import db
from sqlalchemy import *

db = db.getDb()
meta = MetaData(db)

comments = Table('comments', meta,
	Column('id', Integer, primary_key=True),
	Column('content', String),
	Column('commentId', Integer),
	Column('parentCommentId', Integer),
    Column('profileId', Integer),
    Column('talkId', Integer),
	Column('level', Integer),
	Column('name', String(255)),
	Column('replies', Integer),
	Column('score', Integer),
	Column('deleted', Boolean),
	Column('date', DateTime))

comments.create()
